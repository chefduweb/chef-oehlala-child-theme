define([
	
	'jquery',
	'social-share',

], function( $ ){

	/*=====================================================*/
	/*==== Event registers 
	/*=====================================================*/

	//doc ready events:
	$( document ).ready( function(){
	
		setMenu();
		
	});
	
	
	//load events
	$( window ).load( function(){
	
	});
	
	
	//resize events:
	$( window ).resize( function(){
	
	});
	
	
	
	//scroll events
	jQuery(window).scroll(function(){
		
	});
	
	
	
	/*=====================================================*/
	/*==== Functions 
	/*=====================================================*/
	
	
	/**
	 * Set the responsive menu, if applicable
	 * @return void
	 */
	function setMenu(){
	
		//change the jquery selectors to fit the project:
		var toggle = $('.menu-switch');
		var toggleBtn = $('.menu-switch i');
		var menu = $('.mobile-nav');
	
	
		toggle.on('click tap', function(){
			menu.toggleClass( 'fold-out' );
			toggleBtn.toggleClass( 'fa-remove' );
			toggleBtn.toggleClass('fa-bars' );

			toggle.toggleClass( 'clicked-button' );
			$('.background-wrapper').toggleClass( 'active-bg' );
			$('header').toggleClass( 'hide-logo' );
	
			return false;
		});
	
		$( '.menu-item-has-children > a' ).on('click', function( e ){
	
			if( menu.hasClass( 'fold-out' ) ){
	
				e.preventDefault();
	
				var parent = $( this ).parent();
	
				if( parent.hasClass( 'fold-out' ) ){
	
					if( $( this ).attr('href').length > 0 ){
						var _url = $( this ).attr('href');
					}else{
						_url = $( this ).find( 'a' ).attr( 'href' );
					}
	
					window.location.href = _url;
		
				}else{
					
					parent.addClass( 'fold-out' );
		
					return false;
				
				}
			
				return false;
			}
		});
	
	}
	
});